package com.epam.rd.java.basic.task7.db;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {



	public static final String SELECT_ALL_USERS = "select id,login from users";
	public static final String SELECT_USER = "select id,login from users where login=?";
	public static final String SELECT_ALL_TEAMS = "SELECT id,name FROM teams";
	public static final String INSERT_USER = "insert into users values( default, ?)";
	public static final String INSERT_TEAM = "INSERT into teams values( default, ?)";
	public static final String DELETE_TEAM = "delete from teams where id=?";
	public static final String SELECT_TEAM = "SELECT id, name from teams WHERE name=?";
	public static final String INSERT_TEAMS_FOR_USER = "INSERT into users_teams  values  (?,?)";
	public static final String FIND_TEAMS_BY_USER_ID = "SELECT * FROM teams INNER JOIN users_teams ON id = team_id WHERE user_id = ?";
	public static final String UPDATE_TEAM = "UPDATE teams SET name= ? WHERE id=  ?";
	public static final String DELETE_USER = "DELETE from users WHERE id= ?";

	private static DBManager instance;

	private Connection getConnection() throws SQLException {

		return DriverManager.getConnection(getProperties());

		/*return   DriverManager.getConnection("jdbc:mysql://localhost:3306/test2db",
				"root", "123456789");*/
	}
	private String getProperties() {

		Properties appProps = new Properties();
		try (FileInputStream fileInputStream = new FileInputStream("app.properties")) {
			appProps.load(fileInputStream);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return (appProps.getProperty("connection.url"));
	}

	public static synchronized DBManager getInstance() {
		if (instance == null){
			instance = new DBManager();
		}
		return instance;
	}

	private DBManager() {
	}

	//QUERIES AND SHIT
	public List<User> findAllUsers() {
		List<User> users = new ArrayList<>();
		Connection con = null;
		Statement st = null;
		ResultSet rs = null;
		try {
			con = getConnection();
			st = con.createStatement();
			st.executeQuery(SELECT_ALL_USERS);
			rs = st.getResultSet();
				while (rs.next()){
					User u = new User();
					u.setId(rs.getInt("id"));
					u.setLogin(rs.getString("login"));
					users.add(u);
				}

		} catch (SQLException throwables) {
			throwables.printStackTrace();
		} finally {
			closeConnections(con, st, rs);
		}
		return users;
	}



	public boolean insertUser(User user) throws DBException {
		PreparedStatement st = null;
		try (Connection con = getConnection()){
			st = con.prepareStatement(INSERT_USER, Statement.RETURN_GENERATED_KEYS);
			st.setString(1, user.getLogin());

			try {
				int affectedRaws = st.executeUpdate();
				if (affectedRaws == 0){
					throw new SQLException("Inserting user failed, no rows affected");
				}
			} catch (SQLIntegrityConstraintViolationException e){
				System.out.println("user already exists");
			}


		/*	try (ResultSet generatedKeys = st.getGeneratedKeys()) {
				if (generatedKeys.next()) {
					user.setId(generatedKeys.getInt(1));
				} else {
					throw new SQLException("Creating user failed, no ID obtained.");
				}
			}*/

		} catch (SQLException e){
			e.printStackTrace();
			return false;
		} finally {
			try {
				if (st != null)
					st.close();

			}  catch (SQLException e){
				e.printStackTrace();
			}
		}
		return true;
	}

	public boolean deleteUsers(User... users) throws DBException {
		PreparedStatement st = null;
		try (Connection con = getConnection()){
			for ( User user: users) {
				deleteUser(con, user);
			}
		} catch (SQLException e){
			e.printStackTrace();
			return false;
		} finally {
			try {
				if ( st != null)
					st.close();
			} catch (SQLException e){
				e.printStackTrace();
			}
		}
		return true;
	}

	private void deleteUser(Connection con, User user) throws SQLException {
		PreparedStatement st;
		st = con.prepareStatement(DELETE_USER);
		st.setInt(1, user.getId());
		st.execute();
	}

	public User getUser(String login) throws DBException {
		User user = new User();
		Connection con = null;
		PreparedStatement st = null;
		ResultSet rs = null;
		try {
			con = getConnection();
			st = con.prepareStatement(SELECT_USER);
			st.setString(1, login);
			rs = st.executeQuery();
			while (rs.next()){
				user.setId(rs.getInt("id"));
				user.setLogin(rs.getString("login"));
			}

		} catch (SQLException throwables) {
			throwables.printStackTrace();
		} finally {
			closeConnections(con, st, rs);
		}
		return user;
	}

	public Team getTeam(String name) throws DBException {
		Team team = new Team();
		ResultSet rs = null;
		PreparedStatement st =  null;
		Connection conn = null;
		try {
			conn = getConnection();
			st = conn.prepareStatement(SELECT_TEAM);
			st.setString(1,name);
			rs = st.executeQuery();

			while (rs.next()){
				team.setId(rs.getInt("id"));
				team.setName(rs.getString("name"));
			}

		} catch (SQLException e){
			e.printStackTrace();
		} finally {
			closeConnections(conn,st,rs);
		}
		return team;
	}

	public List<Team> findAllTeams() throws DBException {
		List<Team> teams = new ArrayList<>();
		Connection con = null;
		Statement st = null;
		ResultSet rs = null;
		try {
			con = getConnection();
			st = con.createStatement();
			st.executeQuery(SELECT_ALL_TEAMS);
			rs = st.getResultSet();

			while (rs.next()){
				Team t = new Team();
				t.setId(rs.getInt("id"));
				t.setName(rs.getString("name"));
				teams.add(t);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return teams;
	}

	public boolean insertTeam(Team team) throws DBException {
		PreparedStatement st = null;
		try (Connection con = getConnection()){
			st = con.prepareStatement(INSERT_TEAM, Statement.RETURN_GENERATED_KEYS);
			st.setString(1,team.getName());

			try {
				int affectedRows = st.executeUpdate();
				if (affectedRows == 0) {
					throw new SQLException("Failed to add Team, no rows affected");
				}
			}

			 catch (SQLIntegrityConstraintViolationException e){
				throw new SQLException("Inserting failed, Team already exists");
			}

			try (ResultSet generatedKeys = st.getGeneratedKeys()) {
				if (generatedKeys.next()) {
					team.setId(generatedKeys.getInt(1));
				} else {
					throw new SQLException("Creating Team failed, no ID obtained.");
				}
			}
		} catch (SQLException e){
			e.printStackTrace();
			return false;
		} finally {
			try {
				if (st != null)
					st.close();
			} catch (SQLException e){
				e.printStackTrace();
			}
		}
		return true;
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			connection = DriverManager.getConnection(loadProperties().getProperty("connection.url"));
			statement = connection.prepareStatement(INSERT_TEAMS_FOR_USER);
			connection.setAutoCommit(false);
			if (user.getId() == 0){
				statement.setNull(1, Types.INTEGER);
			}else{
				statement.setInt(1, user.getId());
			}
			for (Team t : teams) {
				if (t.getId() == 0){
					statement.setNull(2, Types.INTEGER);
				}else{
					statement.setInt(2, t.getId());
				}
				statement.executeUpdate();
			}
			connection.commit();
			connection.setAutoCommit(true);
		} catch (Exception e) {
			try {
				if (connection != null) {
					connection.rollback();
					connection.setAutoCommit(true);
				}
			} catch (SQLException ex) {
				throw new DBException(e.getMessage(), e);
			}
			throw new DBException(e.getMessage(), e);
		} finally {
			closeConnections(connection, statement, null);
		}
		return true;
	}

	/*private void setRow(Connection con, int userId, int teamId) {
		PreparedStatement st = null;
		try {
		st = con.prepareStatement(INSERT_TEAMS_FOR_USERS);
		st.setInt(1,userId);
		st.setInt(2,teamId);
			st.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (st != null)
					st.close();
			} catch (SQLException e){
				e.printStackTrace();
			}
		}
	}*/

	public List<Team> getUserTeams(User user) throws DBException {
		List<Team> teamList = new ArrayList<>();
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			connection = DriverManager.getConnection(loadProperties().getProperty("connection.url"));
			statement = connection.prepareStatement(FIND_TEAMS_BY_USER_ID);
			if(user.getId() == 0){
				statement.setNull(1, Types.INTEGER);
			}else{
				statement.setInt(1, user.getId());
			}
			resultSet = statement.executeQuery();
			while (resultSet.next()){
				teamList.add(Team.createTeam(resultSet.getString("name")));
			}
		}catch (Exception e){
			throw new DBException(e.getMessage(), e);
		}finally {
			closeConnections(connection, statement, resultSet);
		}
		return teamList;
	}

	public boolean deleteTeam(Team team) throws DBException {

		PreparedStatement st = null;
		try (Connection conn = getConnection()){
			st = conn.prepareStatement(DELETE_TEAM);
			st.setInt(1,team.getId());
			st.execute();
		} catch (SQLException e){
			e.printStackTrace();
			return false;

		} finally {
			try {
				if (st != null)
					st.close();
			} catch (SQLException e){
				e.printStackTrace();
			}
		}
		return true;
	}

	public boolean updateTeam(Team team) throws DBException {
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			connection = DriverManager.getConnection(loadProperties().getProperty("connection.url"));
			statement = connection.prepareStatement(UPDATE_TEAM);
			statement.setString(1, team.getName());
			statement.setInt(2, team.getId());
			statement.executeUpdate();
		}catch (Exception e){
			throw new DBException(e.getMessage(), e);
		}finally {
			closeConnections(connection, statement, null);
		}
		return true;
	}

	private void closeConnections(Connection  con, java.sql.Statement st, ResultSet  rs){

		try {
			if (con != null) con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			if (st != null) st.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			if (rs != null) rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	private static Properties loadProperties() throws IOException {
		Properties properties = new Properties();
		properties.load(new FileInputStream("app.properties"));
		return properties;

	}

}
